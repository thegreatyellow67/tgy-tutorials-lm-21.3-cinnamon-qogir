#!/bin/bash

clear

echo ""
echo "   ██████╗██╗███╗   ██╗███╗   ██╗ █████╗ ███╗   ███╗ ██████╗ ███╗   ██╗"
echo "  ██╔════╝██║████╗  ██║████╗  ██║██╔══██╗████╗ ████║██╔═══██╗████╗  ██║"
echo "  ██║     ██║██╔██╗ ██║██╔██╗ ██║███████║██╔████╔██║██║   ██║██╔██╗ ██║"
echo "  ██║     ██║██║╚██╗██║██║╚██╗██║██╔══██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
echo "  ╚██████╗██║██║ ╚████║██║ ╚████║██║  ██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
echo "   ╚═════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
echo ""
echo "   ██████╗  ██████╗  ██████╗ ██╗██████╗"
echo "  ██╔═══██╗██╔═══██╗██╔════╝ ██║██╔══██╗"
echo "  ██║   ██║██║   ██║██║  ███╗██║██████╔╝"
echo "  ██║▄▄ ██║██║   ██║██║   ██║██║██╔══██╗"
echo "  ╚██████╔╝╚██████╔╝╚██████╔╝██║██║  ██║"
echo "   ╚══▀▀═╝  ╚═════╝  ╚═════╝ ╚═╝╚═╝  ╚═╝"
echo ""
echo "  ===================================================================="
echo "  Script per installare il visualizzatore grafico Glava"
echo "  Scritto da TGY-TUTORIALS il 22/01/2024"
echo "  ===================================================================="

function goto
{
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

echo ""
echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
    s|S)
        goto main
        ;;
    n|N)
        echo "  Termino lo script...a presto!"
        exit 1
        ;;
    *)
        echo "  Tasto non valido. Per favore premi 's' o 'n'."
        sleep 5
        exit 1
        ;;
esac

main:

THEME="qogir"
THEME_FOLDER="lm-21.3-cinnamon-$THEME"
BASE_PATH="$HOME/Scaricati"

echo ""
echo "  Sto installando alcuni pacchetti per la compilazione di glava, un pò di pazienza..."
echo ""

sudo apt install libgl1-mesa-dev libpulse0 libpulse-dev libxext6 libxext-dev libxrender-dev libxcomposite-dev liblua5.3-dev liblua5.3-0 lua-lgi lua-filesystem libobs0 libobs-dev meson build-essential gcc ccache -y &> /dev/null

sudo ldconfig
sudo ccache -c

echo ""
echo "  Compilo glava, un pò di pazienza..."
echo ""

cd ${BASE_PATH}/${THEME_FOLDER}/glava-src-gitlab
#meson build --reconfigure --prefix /usr
meson build --prefix /usr
ninja -C build
sudo ninja -C build install

