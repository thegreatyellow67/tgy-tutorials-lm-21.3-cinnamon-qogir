conky.config = {
--==============================================================================

--  This theme is for conky version 1.10.8 or newer
--
--  AUVA_DARK2 (ITALIAN version)
--  ( A part of Scorpio Conky themes pack )
--
--  Original author : Closebox73
--  Created         : 2022/04/22
--  Version         : 2.5
--  Variant         : Celsius
--  Modified by     : TheGreatYellow67
--  Modified date   : 2024/01/22
--
--  License         : Distributed under the terms of GPLv3
--  Notes           : Created on 1920x1080 Monitor

--==============================================================================

-- Size and Position settings --
  alignment = 'bottom_left',
  gap_x = 30,
  gap_y = 100,
  maximum_width = 420,
  minimum_height = 350,
  minimum_width = 420,
  
-- Text settings --
  use_xft = true,
  font = 'Roboto:light:size=9',
  override_utf8_locale = true,
  
-- Color Settings --
  default_color = '#FFFFFF',
  default_outline_color = 'white',
  default_shade_color = 'white',
  color1 = '#4881C8',
  
-- Window Settings --
  background = false,
  draw_blended = false,
  border_width = 1,
  draw_borders = false,
  draw_graph_borders = false,
  draw_outline = false,
  draw_shades = false,
  own_window = true,
  own_window_class = 'Conky',
  stippled_borders = 0,
  
-- To get REAL TRANSPARENCY --
  own_window_type = 'desktop',
  own_window_transparent = false,
  own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager,below',
  own_window_colour = '#000000',
  own_window_argb_visual = true,
  own_window_argb_value = 0,
  
-- Others --
  cpu_avg_samples = 2,
  net_avg_samples = 2,
  double_buffer = true,
  out_to_console = false,
  out_to_stderr = false,
  extra_newline = false,
  update_interval = 1,
  uppercase = false,
  use_spacer = 'none',
  show_graph_scale = false,
  show_graph_range = false,
lua_load = '~/.config/conky/Auva-Dark/scripts/lua/rings-v1.2.1.lua',
lua_draw_hook_pre = 'ring_stats',
}

conky.text = [[
${execi 600 ~/.config/conky/Auva-Dark/scripts/weather-v2.0.sh}\
${image ~/.config/conky/Auva-Dark/res/bcg.png -s 110x46 -p 0,250}\
${font Roboto:bold:size=20}Oggi è ${color1}${time %A}
${voffset 10}${offset 0}${color}${font Big john:size=22}Spero che...
${voffset 5}${execi 600 ~/.config/conky/Auva-Dark/scripts/Greeting.sh}
${voffset 5}stia andando bene,
${voffset 5}${execi 1200 whoami}!
${offset 0}${voffset 0}${font Roboto:light:size=11}La temperatura attuale è : ${execi 100 cat ~/.cache/weather.json | jq '.main.temp' | awk '{print int($1+0.5)}'}°C
Condizioni meteo : ${execi 100 ~/.config/conky/Auva-Dark/scripts/weather-description.sh}
Località : ${execi 100 cat ~/.cache/weather.json | jq -r '.name'}, ${execi 100 cat ~/.cache/weather.json | jq -r '.sys.country'}
${voffset 15}${goto 24}${color white}${font Roboto:bold:size=20}${time %H:%M}
${offset 50}${voffset 10}${color}${font Roboto:size=8}Cpu:${goto 165}Mem:${goto 275}Home:${goto 320}System:
${offset 50}${voffset -2}${font Roboto:bold:size=9}${cpu cpu0}%${goto 165}${memperc}%${goto 275}${fs_used_perc /home}%${goto 320}${fs_used_perc /}%
]]
