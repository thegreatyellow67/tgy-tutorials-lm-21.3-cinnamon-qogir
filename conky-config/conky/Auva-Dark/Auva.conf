conky.config = {
--==============================================================================

--  This theme is for conky version 1.10.8 or newer
--
--  AUVA_DARK (ITALIAN version)
--  ( A part of Scorpio Conky themes pack )
--
--  Original author : Closebox73
--  Created         : 2022/04/22
--  Version         : 2.5
--  Variant         : Celsius
--  Modified by     : TheGreatYellow67
--  Modified date   : 2024/01/22
--
--  License........ : Distributed under the terms of GPLv3
--  Notes           : Created on 1920x1080 Monitor

--==============================================================================

-- Size and Position settings --
  alignment = 'top_right',
  gap_x = 0,
  gap_y = 100,
  maximum_width = 180,
  minimum_height = 180,
  minimum_width = 180,
  
-- Text settings --
  use_xft = true,
  font = 'Roboto:size=9',
  override_utf8_locale = true,
  
-- Color Settings --
  default_color = 'white',
  default_outline_color = 'black',
  default_shade_color = 'black',
  color1 = '#D71E6C',
  
-- Window Settings --
  background = false,
  draw_blended = false,
  border_width = 0,
  draw_borders = false,
  draw_graph_borders = false,
  draw_outline = false,
  draw_shades = false,
  own_window = true,
  own_window_class = 'Conky',
  stippled_borders = 0,

-- To get REAL TRANSPARENCY --
  own_window_type = 'desktop',
  own_window_transparent = false,
  own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager,below',
  own_window_colour = '#000000',
  own_window_argb_visual = true,
  own_window_argb_value = 0,
  
-- Others --
  cpu_avg_samples = 2,
  net_avg_samples = 2,
  double_buffer = true,
  out_to_console = false,
  out_to_stderr = false,
  extra_newline = false,
  update_interval = 1,
  uppercase = false,
  use_spacer = 'none',
  show_graph_scale = false,
  show_graph_range = false
}

conky.text = [[
${image ~/.config/conky/Auva-Dark/res/bg_darker.png -s 160x160}\
${image ~/.config/conky/Auva-Dark/res/bg.png -s 160x160 -p 20,20}\
${offset 40}${voffset 30}${color0}${font Roboto:bold:size=40}${time %d}.${font}
${offset 40}${voffset 8}${color0}${font Roboto:bold:size=12}${time %^B}${font}
${offset 40}${voffset 25}${color0}${font Roboto:bold:size=15}${time %^Y}${font}
]]
