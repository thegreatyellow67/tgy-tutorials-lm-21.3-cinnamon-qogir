#!/bin/bash

Greeting=$(date +%H)
cat $0 | grep $Greeting | sed 's/# '$Greeting' //'
# 
# --------------------------------------------------------------------------------
# 00 la tua Mezzanotte
# 01 la tua Mattina
# 02 la tua Mattina
# 03 la tua Mattina
# 04 la tua Mattina
# 05 la tua Mattina
# 06 la tua Mattina
# 07 la tua Mattina
# 08 la tua Mattina
# 09 la tua Mattina
# 10 la tua Mattina
# 11 il tuo Mezzogiorno
# 12 il tuo Mezzogiorno
# 13 il tuo Pomeriggio
# 14 il tuo Pomeriggio
# 15 il tuo Pomeriggio
# 16 il tuo Pomeriggio
# 17 il tuo Pomeriggio
# 18 la tua Sera
# 19 la tua Sera
# 20 la tua Sera
# 21 la tua Sera
# 22 la tua Sera
# 23 la tua Sera
