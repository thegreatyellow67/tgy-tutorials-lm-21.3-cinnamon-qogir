NOTA: i files di configurazione sono installati tramite install-res.sh

- Installazione di Plank:
sudo apt install plank

- Aggiungere Plank in:
Impostazioni di sistema -> Applicazioni d'avvio
Ritardo: 5 secondi

NOTA: il persorso delle icone di separator.desktop e separator1.desktop che si trovano in ~/.local/share/applications viene cambiato tramite lo script install-res.sh
