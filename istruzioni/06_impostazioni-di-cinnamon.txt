- Impostazioni di sistema -> Selezione carattere
Carattere predefinito:					Inter Medium 10
Tipo di carattere del desktop:			Inter Medium 9
Carattere per i documenti:				Inter Regular 10
Carattere a larghezza fissa:			CascaydiaCove Nerd Font Regular 10
Carattere del titolo delle finestre:	Inter SemiBold 9

- Impostazioni di sistema -> Finestre -> Barra del titolo -> Disposizione dei pulsanti -> Sinistra

- Modifica del layout dei bottoni della finestra:
Tramite dconf-editor, modificare il layout dei bottoni, il percorso è:
org / cinnamon / desktop / wm / preferences
Custom value: close,minimize,maximize:

- Impostazioni di sistema -> Finestre -> Comportamento -> Posizionamento delle nuove finestre aperte -> Centrato

- Impostazioni di sistema -> Finestre -> Alt-Tab -> Stile selettore Alt-Tab -> Timeline (3D)

- Impostazioni di sistema -> Temi
Puntatore del mouse:	Qogir-dark
Applicazioni:			Qogir-Dark
Icone:					Qogir-dark
Desktop:				Qogir-dark
