- Impostazioni del pannello:
	Altezza del pannello: 28

	Zona sinistra:
	Dimensione dei caratteri: 9.0pt
	Dimensione delle icone colorate: 22px
	Dimensione delle icone simboliche: 22px
	Zona centrale:
	Dimensione dei caratteri: 9.0pt
	Dimensione delle icone colorate: 16px
	Dimensione delle icone simboliche: 16px
	Zona destra:
	Dimensione dei caratteri: 9.0pt
	Dimensione delle icone colorate: 16px
	Dimensione delle icone simboliche: 16px

- Scaricare le seguenti applet:
	- Nome area di lavoro
	- Meteo
	- Indicatore temperatura CPU
	- Indicatore dei tasti di blocco con notifiche
	- Radio++
	- Screenshot+Record Desktop
	- Gestore Sessione

- Altre applet di serie:
	- Cambio area di lavoro

- Estensioni:
	- Pannelli trasparenti
	- Opacizza finestre
	- Horizontal OSD

- Applet Cambio area di lavoro:
aggiungere nuove aree di lavoro (in totale 6), poi cambiare i nomi:
web, chat, terminali, sviluppo, posta, multimedia

- Applet Calendario:
Utilizza un formato della data personalizzato: ATTIVO
Formato della data: %a %d %b / %H:%M
Formato della data per il tooltip: %A %d %B, %H:%M

- Applet Gestore Sessione:
Dimensione Icona (pixel): 16

- Applet Radio++:
Al primo click far installare mpv

NOTA BENE: ripristinare le impostazioni che si trovano nei files:
~/Scaricati/lm-21.3-cinnamon-qogir/applets-confs
~/Scaricati/lm-21.3-cinnamon-qogir/extensions-confs

- Ordine delle applets nel pannello:
Zona sinistra:
1. Menù (menu@cinnamon.org)
2. Separatore
3. Elenco finestre raggruppate (grouped-window-list)
Zona centrale:
4. Nome Area di Lavoro (workspace-name)
5. Separatore
6. Cambio area di lavoro (workspace-switcher)
7. Separatore
8. Indicatore dei tasti
Zona destra:
9. Radio++ (radio@driglu4it)
10. ScreenShot+Record Desktop (ScreenShot+RecordDesktop)
11. Diodon (no applet, solo icona systray della app)
12. Ulauncher (no applet, solo icona systray della app)
13. Update Manager (no applet, solo icona systray della app)
14. Unità rimovibili (removable-drives)
15. Gestione reti (network)
16. Audio (sound)
17. Separatore
18. Nel PC fisico aggiungi anche Indicatore Temperatura CPU
19. Meteo (weather)
20. Separatore
21. Calendario (calendar)
22. Gestore Sessione (sessionManager)

- Avviare Plank
